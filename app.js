// Move element of array from one place to another
// It's not the fastest, but it's reactive and short :))
// https://jsperf.com/array-prototype-move
Array.prototype.move = function(from, to) {
	if(from > to) {
		var deleted = this.splice(from, 1)[0];
		this.splice(to, 0, deleted);
	} else if(from < to) {
		var deleted = this.splice(from, 1)[0];
		this.splice(to-1, 0, deleted);
	}
}

var app = new Vue({
	el: '#app',
	data: {
		elCardPlaceholder: null,
		isDragging: false,
		card: {
			editing: false,
			id: 0,
			title: '',
			description: ''
		},
		lists: sampleLists,
		cards: sampleCards
	},
	computed: {
		listsCards: function () {
			var arr = Array(this.lists.length);
			for (i = 0; i < arr.length; i++) {
				arr[i] = [];
			}
			for (i = 0; i < this.cards.length; i++) {
				arr[this.cards[i].listID].push(i);
			}
			return arr;
		}
	},
	methods: {
		getCardPlaceholder: function() {
			if (!this.elCardPlaceholder) { // Create if not exists
				this.elCardPlaceholder = document.createElement('div');
				this.elCardPlaceholder.className = "card-placeholder";
			}
			return this.elCardPlaceholder;
		},
		dragstart: function(e) {
			this.isDragging = true;
			e.dataTransfer.setData('text/plain', e.target.dataset.id);
			e.dataTransfer.dropEffect = "copy";
			e.target.classList.add('dragging');
		},
		dragend: function(e) {
			e.target.classList.remove('dragging');
			this.elCardPlaceholder && this.elCardPlaceholder.remove();
			this.elCardPlaceholder = null;
		},
		dragover: function (e) {
			e.preventDefault();
			e.dataTransfer.dropEffect = "move";
			if (e.target.className === "list") { // List
				e.target.appendChild(this.getCardPlaceholder());
			} else if (e.target.className.indexOf('card') !== -1) { // Card
				e.target.parentNode.insertBefore(this.getCardPlaceholder(), e.target);
			}
		},
		drop: function (e) {
			e.preventDefault();
			if (!this.isDragging) return false;
			this.isDragging = false;
			var cardID = +e.dataTransfer.getData('text');
			var nextCardID = app.cards.length;
			if (e.target.className === 'list') { // Dropped on List
				app.cards[cardID].listID = e.target.dataset.id;
			} else { // Dropped on Card Placeholder
				app.cards[cardID].listID = e.target.parentNode.dataset.id;
				if (e.target.nextSibling !== null) {
					nextCardID = e.target.nextSibling.dataset.id;
				}
			}
			console.log(nextCardID);
			app.cards.move(cardID, nextCardID);
		},
		addTodo: function(e) {
			if(e.target.value.length > 0) {
				this.cards.push({ listID: 0, title: e.target.value });
				e.target.value = '';
			}
		},
		addList: function(e) {
			if(e.target.value.length > 0) {
				this.lists.push({ title: e.target.value });
				e.target.value = '';
			}
		},
		editCard: function(e) {
			this.card.id = e.target.closest('.card').dataset.id; // closest('.card') - на случай если щелкнули по иконкам на card
			this.card.title = app.cards[this.card.id].title;
			this.card.description = app.cards[this.card.id].description;
			this.card.editing = true;
			Vue.nextTick(function () {
				$('.window-title-input').focus();
			});
		},
		saveCard: function() {
			app.cards[this.card.id].title = this.card.title;
			app.cards[this.card.id].description = this.card.description;
			this.card.editing = false;
		}
	}
})

$('.window').draggable();